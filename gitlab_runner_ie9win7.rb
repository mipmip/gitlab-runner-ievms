#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'
require './lib/ievms_gitlab_runner'

################# CONFIGUATION #############
# Displayed in your Runners Section in the Gitlab CI Server
registration_token = 'f9b04696c2f3ce3766d8b0b0a440ed'

# The URL of your GitLab CI Server
gitlab_ci_url = 'https://gitlab.com/ci'

# The runner will appear in GitLab CI under this name
runner_name = 'ie9win7'

# the runner will take jobs for these tags is it take shared jobs
runner_tag_list = 'window-tests, ie-tests'

############ END CONFIGURATION #############


provision = ProvisionIE.new "IE9 - Win7"
provision.headless = true
provision.start true

# Install rktools with ntrights.exe to set permissions to allow gitlab runner
# to start as server as IEUser
provision.choco_install('rktools.2003')
provision.set_service_permissions

# you want to install more software? ... Check out chocolatey.org what is
# available and install it with the line:
# provision.choco_install('mypackage')

# If you have private repo's you need a key to authenticate
provision.add_rsa_key File.join(File.dirname(__FILE__),'config', 'id_dsa')

# Install the gitlab runner and start the service. You can replace v0.6.0 with
# latest or another version. The config.toml is created with the configuration
# variables from above.
provision.gitlab_ci_multi_runner_download 'v0.6.0', '386'
provision.gitlab_ci_multi_runner_upload_config_toml registration_token, gitlab_ci_url, runner_name, runner_tag_list
provision.gitlab_ci_multi_runner_install_service
provision.gitlab_ci_multi_runner_start_service
provision.gitlab_ci_multi_runner_query_service
