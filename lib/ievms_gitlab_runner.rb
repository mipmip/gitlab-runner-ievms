#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'
require 'erb'
require 'ostruct'
require 'ievms/windows_guest'

class ProvisionIE < Ievms::WindowsGuest

  # Install ruby and git stuff, the reason why we wrote ievms-ruby
  def install_gems
    run_command_as_admin "NetSh Advfirewall set allprofiles state off"

    %w( bundler watir-webdriver ).each do |gem|
      puts run_command "gem install #{gem} --no-ri --no-rdoc --conservative"
    end

    run_command_as_admin "NetSh Advfirewall set allprofiles state on"
  end

  def add_rsa_key(path_key)
    run_command 'if not exist C:\Users\IEUser\.ssh mkdir C:\Users\IEUser\.ssh'
    upload_file_to_guest(path_key, 'C:\Users\IEUser\.ssh\id_rsa')
  end

  # download gitlab multirunner
  def gitlab_ci_multi_runner_download(runner_version='latest', arch='386')

    runner_path = File.expand_path("../cache/gitlab-ci-multi-runner.exe-#{runner_version}", File.dirname(__FILE__))

    if ! File.exists?(runner_path)
      `curl -o #{runner_path} https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/#{runner_version}/binaries/gitlab-ci-multi-runner-windows-#{arch}.exe`
    end

    run_command_as_admin 'if not exist C:\Multi-Runner mkdir C:\Multi-Runner'
    upload_file_to_guest(runner_path, 'C:\Multi-Runner\gitlab-ci-multi-runner.exe')
  end

  # stop and uninstall as service
  def gitlab_ci_multi_runner_uninstall
    gitlab_ci_multi_runner_stop_service
    run_command_as_admin "C:\\Multi-Runner\\gitlab-ci-multi-runner uninstall"
    run_command_as_admin "del C:\\Multi-Runner\\config.toml"
  end

  # register at gitlab server
  # FIXME not working, not creating a config.toml
  def gitlab_ci_multi_runner_register(token, url, runner_name, taglist)
    run_command "C:\\Multi-Runner\\gitlab-ci-multi-runner register --non-interactive"\
      " --registration-token #{token} --url #{url} --name #{runner_name}"\
      " --shell shell --tag-list #{taglist} -c c:\\Multi-Runner\\config.toml"
  end

  # stop runner service
  def gitlab_ci_multi_runner_stop_service
    run_command_as_admin "C:\\Multi-Runner\\gitlab-ci-multi-runner stop"
  end

  # delete runner service
  def gitlab_ci_multi_runner_delete_service
    run_command_as_admin "sc delete gitlab-runner"
    run_command_as_admin "REG DELETE HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\gitlab-runner /f"
  end

  # start runner
  def gitlab_ci_multi_runner_upload_config_toml(token, url, runner_name, taglist)

    namespace = OpenStruct.new(token: token, url: url, runner_name: runner_name, taglist: taglist)

    conf_template = File.read(File.expand_path('../config/config.toml.erb', File.dirname(__FILE__)))
    renderer = ERB.new(conf_template)
    conf = renderer.result(namespace.instance_eval { binding })

    upload_string_as_file_to_guest(conf, 'C:\Multi-Runner\config.toml')
  end

  # set run as service logon permissions
  def set_service_permissions
    run_command_as_admin '"C:\Program Files\Windows Resource Kits\Tools\ntrights.exe" +r SeServiceLogonRight -u IEUser'
  end

  # unset run as service logon permissions
  def revoke_service_permissions
    run_command_as_admin '"C:\Program Files\Windows Resource Kits\Tools\ntrights.exe" -r SeServiceLogonRight -u IEUser'
  end

  # install as service
  def gitlab_ci_multi_runner_install_service

    # With Gitlab CI Multi Runner this command does install a service but it can not be started
    # run_command_as_admin "C:\\Multi-Runner\\gitlab-ci-multi-runner install password 'Passw0rd!'"

    run_command_as_admin "sc create gitlab-runner "\
      " binpath= \"C:\\Multi-Runner\\gitlab-ci-multi-runner.exe "\
      " run --working-directory C:\\Users\\IEUser "\
      " --config C:\\Multi-Runner\\config.toml --service gitlab-runner"\
      " --syslog\" type= own start= auto obj= \".\\IEUser\" password= \"Passw0rd!\""
  end

  # start runner
  def gitlab_ci_multi_runner_start_service
    run_command_as_admin "C:\\Multi-Runner\\gitlab-ci-multi-runner start"
  end

  # check is runner service is running
  def gitlab_ci_multi_runner_query_service
    print run_command 'sc.exe query gitlab-runner'
  end
end
